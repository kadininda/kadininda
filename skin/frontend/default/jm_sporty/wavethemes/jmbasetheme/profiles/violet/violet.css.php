<?php
	$mageFilename = "../../../../../../../../app/Mage.php";
	require_once $mageFilename; 
	umask(0);
	Mage::app();
	$baseconfig = Mage::helper("jmbasetheme")->getactiveprofile();
	header("Content-type: text/css; charset: UTF-8");
?>

/* Base settings */
body#bd {
	background-color: <?php echo $baseconfig["bgolor"]; ?>;
	background-image:url("images/<?php echo $baseconfig["bgimage"]; ?>");
}

#ja-header {
	background-color: <?php echo $baseconfig["headerolor"]; ?>;
	background-image:url("images/<?php echo $baseconfig["headerimage"]; ?>");
}

h1#logo a  {
	background-image:url("images/<?php echo $baseconfig["logo"]; ?>") !important;
}

#ja-footer {
	background-color: <?php echo $baseconfig["footerolor"]; ?>;
	background-image:url("images/<?php echo $baseconfig["footerimage"]; ?>");
}

.jm-slide-number .jm-slide-thumbs-handles span.active, 
.jm-slide-number .jm-slide-thumbs-handles span.hover {
  background-image:url("images/<?php echo $baseconfig["slidethumbshandles"]; ?>");
}

/* Layout colors */

a,
a:hover,
a:focus,
a:active,
.shop-access a:hover,
.shop-access a:active,
.shop-access a:focus,
.validation-advice,
#jm-error .disc a,
#ja-mycart .gotocart,
#productsize span.active,
.btn-action-button .btn-empty ,
.btn-action-button .btn-update,
.btn-action-button .btn-continue,
.multiple-checkout .button-shipping,
.checkout-progress .back-link a,
.checkout-actions .back-link a,
.multiple-checkout .back-link a,
.checkout-actions  .btn-update,
.multiple-checkout .btn-update,
.block-compare .actions a:active,
.block-compare .actions a:focus,
.block-compare .actions a:hover,
.cart-collaterals .totals .price,
.jm-slide-desc h3 span.text-color2,
#ja-botsl .block-list ol li a:active,
#ja-botsl .block-list ol li a:focus,
#ja-botsl .block-list ol li a:hover,
#ja-botsl .block-list ol li:hover a,
.jm-megamenu ul.level1 li.mega a.mega:active,
.jm-megamenu ul.level1 li.mega a.mega:focus,
.jm-megamenu ul.level1 li.mega a.mega:hover,
.jm-megamenu .category-products .product-name a:active,
.jm-megamenu .category-products .product-name a:focus,
.jm-megamenu .category-products .product-name a:hover,
.review-product-list .add-to-links li a:hover,
.product-options-bottom .add-to-links li a:hover,
.products-list .product-shop .add-to-links li a:hover,
.products-list .product-shop .f-fix h2.product-name a:active,
.products-list .product-shop .f-fix h2.product-name a:focus,
.products-list .product-shop .f-fix h2.product-name a:hover,
.multiple-checkout .place-order .grand-total .price,
.jm-megamenu ul.level1 li.mega div.group-title a.mega {
	color: <?php echo $baseconfig["color"] ?>;
}

.opc .allow .number  {
	border-color: <?php echo $baseconfig["color"] ?>;
	background-color: <?php echo $baseconfig["color"] ?>;
}

.opc .active .number,
.box-account ol .number,
.checkout-progress li.active {
	background-color: <?php echo $baseconfig["color"] ?>;
}
#productsize span.active,
.has-toggle .btn-toggle.active + .inner-toggle,
#ja-mycart .inner-toggle {
	border-color: <?php echo $baseconfig["color"] ?>;
}

#ja-mycart {
	background-image:url("images/<?php echo $baseconfig["mycart"]; ?>");
}

.block .actions a,
.products-grid li .actions .link-compare:hover, 
.products-grid li .actions .link-compare:focus,
.products-grid li .actions .link-wishlist:hover, 
.products-grid li .actions .link-wishlist:focus,
#ja-botsl .block-subscribe .button,
.mass-button-actions .link-wishlist:hover, 
.mass-button-actions .link-wishlist:focus,
.mass-button-actions .link-compare:hover, 
.mass-button-actions .link-compare:focus,
button.btn-checkout,
.product-essential .product-shop .add-to-cart button,
.block-compare .actions button,
#ja-botsl .block-subscribe .button,
.block-cart .actions button,
.products-list .product-shop .desc-info .button-set button,
.button.btn-cart {
	background-image:url("images/<?php echo $baseconfig["button"]; ?>");
}

.mass-button-actions button:hover,
.mass-button-actions button:focus,
.products-grid li .actions button:hover,
.products-grid li .actions button:focus,
.mass-button-actions button.button:hover,
.mass-button-actions button.button:focus,
.products-grid li .actions button.button:hover,
.products-grid li .actions button.button:focus,
.jm-slider li:hover .mass-button-actions .out-of-stock:hover, 
.jm-slider li:hover .mass-button-actions .out-of-stock:focus,
.products-grid li:hover .actions .out-of-stock:hover, 
.products-grid li.actions:hover .out-of-stock:focus {
	background-image:url("images/<?php echo $baseconfig["button"]; ?>") !important;
}

button.btn-checkout:focus, 
button.btn-checkout:hover,
.block .actions a:hover,
.block-cart .actions button:hover,
.product-essential .product-shop .add-to-cart button:hover,
#ja-botsl .block-subscribe .button:hover,
.products-list .product-shop .desc-info .button-set button:hover,
.button.btn-cart:hover {
	background-image:url("images/<?php echo $baseconfig["buttonhover"]; ?>");
}

ul.ja-tab-navigator li.active, 
ul.ja-tab-navigator li.firstactive, 
ul.ja-tab-navigator li.lastactive,
.sporty .jm-tabs-title-top ul.jm-tabs-title li.active, 
.sporty .jm-tabs-title-top ul.jm-tabs-title li.firstactive, 
.sporty .jm-tabs-title-top ul.jm-tabs-title li.lastactive {
	background-image:url("images/<?php echo $baseconfig["tabactive"]; ?>");
}
ul.ja-tab-navigator li.active a + span,
.sporty .jm-tabs-title-top ul.jm-tabs-title li.active h3 + span {
  background-image:url("images/<?php echo $baseconfig["tabarrowactive"]; ?>");
}

.jm-mask-desc .readmore a {
	background-image:url("images/<?php echo $baseconfig["readmoreslide"]; ?>");
}

.jm-prev:hover, .jm-prev:focus {
  background-image:url("images/<?php echo $baseconfig["sliderhoverpre"]; ?>");
}

.jm-next:hover, .jm-next:focus {
  background-image:url("images/<?php echo $baseconfig["sliderhovernext"]; ?>");
}