(function($) {

    var defaults = {
        attribute:"size",
        mainmainproduct:null  
    }

    var jmproductdetail = function(elm,options){
        this.selectobj = {};
    	  this.element = $(elm);
    	  this.options = $.extend({}, defaults, options);
		    this.initialize();
    }

    jmproductdetail.prototype = {
        initialize:function(){
            data = {};
            dls = this.element.find("dl");
            this.selectobj = $("select#attribute"+this.options.attributeid);
            newselect = $("<div/>",{
                id:"product"+this.options.attribute
            });
            if(this.selectobj && this.selectobj.length){

                $.each(this.options.attribute_arr,$.proxy(function(i,n){

                     newspan = $("<span/>",{
                       value:i
                     });

                     if(this.options.attribute_qtys[i] > 0 ){
                       newspan.css({cursor:"pointer"});
                       newspan.bind("click",$.proxy(function(e){
                            newselect.find("span").removeClass("active");
                            $(e.target).addClass("active"); 
                            this.options.attribute_qtys[i];
                            this.selectobj.val(i);                       
                            url = baseurl+"/jmproductdetail/index/index";
                            data["mainproduct"] = this.options.mainproduct;
                            data["attribute"] = this.options.attribute;
                            data["attributevalue"] = i;
                            this.submiturl(url,data);
                       },this));
                     }else{
                       newspan.addClass("disabled"); 
                     }
                     newspan.html(n);
                     newspan.prependTo(newselect);

                },this));

                this.element.append(newselect);
                this.selectobj.hide();
            }    
        },

        submiturl:function(url,data){
            $.ajax({
                  type: 'POST',
                  url: url,
                  data: data,
                  success: function(result){

                  }
                  ,
                  dataType: "json"
                
            });
        }

    }
    $.fn.jmproductdetail = function(options){
    	new jmproductdetail(this,options);
    }
    
})(jQuery)