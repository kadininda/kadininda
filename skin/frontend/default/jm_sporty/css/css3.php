<?php
    header('Content-type: text/css; charset: UTF-8');
    header('Cache-Control: must-revalidate');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 3600) . ' GMT');
    $url = $_REQUEST['url'];
?>
#ja-header,
.jm-slide-desc h3,
.block-banner li img
{behavior: url(<?php echo $url; ?>css/css3.htc);position:relative;}

.jm-slider li:hover .mass-color {
	border:1px soild #ddd;
}

.jm-slider li:hover .mass-color-inner {
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 5px 5px 5px 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
    box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
    display: block;
    height: 110%;
    margin: 0 5px;
    width: 98%;
	border:1px soild #ddd;
	position: absolute!important;
}

.products-grid li:hover .mass-color {
	border: 1px solid #ddd;
	border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
	display: block;
	height: 142%;
	left: -1px;
	-moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.2);
	top: -10px;
	-webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.2);
	width: 100%;
	z-index: -1;
	position: absolute!important;
}

