<?php
/*------------------------------------------------------------------------
# $JA#PRODUCT_NAME$ - Version $JA#VERSION$ - Licence Owner $JA#OWNER$
# ------------------------------------------------------------------------
# Copyright (C) 2004-2009 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites: http://www.joomlart.com - http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

class JoomlArt_JmTabs_Model_System_Config_Source_ListStyle
{
    public function toOptionArray()
    {
        $defaultTheme = Mage::getStoreConfig('design/theme/default', Mage::app()->getWebsite(true)->getDefaultStore()->getCode());
        $themesDir = BP . DS . 'skin' . DS . 'frontend'  . DS . 'default' . DS . $defaultTheme  . DS . 'joomlart' . DS . 'jmtabs' . DS . 'themes';
        if (!is_dir($themesDir)){
            $themesDir = BP . DS . 'skin' . DS . 'frontend'  . DS . 'default' . DS . 'default'  . DS . 'joomlart' . DS . 'jmtabs' . DS . 'themes';
        }

        $defaults =array();
        $a = array();
        $d = dir($themesDir);
        if (false!== $d){
            while (false !== ($entry = $d->read())) {
                if ($entry != "." && $entry != "..") {
                    $defaults[] = $entry;
                    $a[] = $entry;
                }
            }
        }
        reset($a);

        foreach ($a as $v) {
            $arr[] = array('value' => $v, 'label' => ucfirst($v));
        }

        return $arr;
    }
}
