<?php

/*------------------------------------------------------------------------
# $JA#PRODUCT_NAME$ - Version $JA#VERSION$ - Licence Owner $JA#OWNER$
# ------------------------------------------------------------------------
# Copyright (C) 2004-2009 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites: http://www.joomlart.com - http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

class JoomlArt_Jmtabs_Helper_Data extends Mage_Core_Helper_Abstract
{

	protected $defaultStoreCode = null;
	protected $activeStoreCode = null;

	public function __construct()
	{
		//initial default store code
		$this->defaultStoreCode = Mage::app()->getWebsite(true)->getDefaultStore()->getCode();

		//initial current store code
		$this->activeStoreCode = Mage::app()->getStore()->getCode();
	}

	public function get($var, $attributes)
	{
		$value = null;
		if (isset($attributes[$var])) {
			$value = $attributes[$var];
		} else {
			$storeCode = ($this->activeStoreCode) ? $this->activeStoreCode : $this->defaultStoreCode;
			$configGroups = Mage::getStoreConfig("joomlart_jmtabs", $storeCode);
			foreach ($configGroups as $configs) {
				if (isset($configs[$var])) {
					$value = $configs[$var];
					break;
				}
			}
		}

		return $value;
	}
}
