<?php

/*------------------------------------------------------------------------
# $JA#PRODUCT_NAME$ - Version $JA#VERSION$ - Licence Owner $JA#OWNER$
# ------------------------------------------------------------------------
# Copyright (C) 2004-2009 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites: http://www.joomlart.com - http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

class JoomlArt_Jmtabs_Block_Core extends Mage_Core_Block_Template
{
	protected $_config = array();
	protected $_setTitles = '';
	protected $_setContents = '';

	public function __construct($attributes = array())
	{
		$helper = Mage::helper('joomlart_jmtabs/data');

		$this->_config['show'] = $helper->get('show', $attributes);
		if (!$this->_config['show']) return;

		parent::__construct();

		$this->_config['title'] = $helper->get('title', $attributes);
		$this->_config['style'] = $helper->get('style', $attributes);
		$this->_config['position'] = $helper->get('position', $attributes);
		$this->_config['duration'] = $helper->get('duration', $attributes);
		$this->_config['animation'] = $helper->get('animation', $attributes);

		$this->_config['height'] = $helper->get('height', $attributes);
		if(!$this->_config['height'])
			$this->_config['height'] = 'auto';

		$this->_config['width'] = $helper->get('width', $attributes);
		if(!$this->_config['width'])
			$this->_config['width'] = 'auto';

		$this->_config['event'] = $helper->get('event', $attributes);
		$this->_config['skipanim'] = $helper->get('skipanim', $attributes);
	}

	protected function _prepareLayout()
	{
		if ($this->_config['show']) {
			$headBlock = $this->getLayout()->getBlock('head');
			$headBlock->addCss('joomlart/jmtabs/themes/' . $this->_config['style'] . '/style.css');
		}

		return parent::_prepareLayout();
	}

	public function addTabs_staticblock($title = '', $identifier = '')
	{
		if (!$title || !$identifier) {
			return false;
		}
		$this->_setTitles[] = $title;
		$model_cms = Mage::getModel('cms/block');
		$model = Mage::getModel('joomlart_jmtabs/listData');
		$block_id = $model->_getBlockId($model_cms, $identifier);

		$this->_setContents[] = $this->get_Content_Static_Block($block_id);
	}

	public function addTabs_block($title = '', $block = '', $template = '', $alias = '')
	{
		if (!$title || !$block || !$template) {
			return false;
		}

		if ($alias == '') {
			$alias = 'tab-' . uniqid();
		}

		$this->setChild($alias,
			$this->getLayout()->createBlock($block, $alias)
				->setTemplate($template)
		);

		$this->_setTitles[] = $title;
		$this->_setContents[] = $this->getChildHtml($alias);
	}

	public function _toHtml()
	{
		if (!$this->_config['show']) return;

		$this->assign('config', $this->_config);

		$this->assign('titles', $this->_setTitles)
			->assign('contents', $this->_setContents)
			->assign('tabsid', 'myTabs-' . uniqid());

		if (!$this->getTemplate()) {
			$this->setTemplate('joomlart/jmtabs/tabs.phtml');
		}

		return parent::_toHtml();
	}

	public function get_Content_Static_Block($block_id)
	{
		$html = '';
		if ($block_id) {
			$block = Mage::getModel('cms/block')
				->setStoreId(Mage::app()->getStore()->getId())
				->load($block_id);

			if (!$block->getIsActive()) {
				$html = '';
			} else {
				$helper = Mage::helper('cms');
				$processor = $helper->getBlockTemplateProcessor();
				$content = $processor->filter($block->getContent());

				$processor = Mage::getModel('core/email_template_filter');
				$html = $processor->filter($content);
			}
		}

		return $html;
	}

}